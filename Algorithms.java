import java.util.ArrayList;

public class Algorithms {

    /* Bubble Sort
     * Worst-case time complexity: O(n^2)
     * Average-case time complexity: O(n^2)
     * Best-case time complexity (with optimized implementation): O(n)
     * */
    public static <T extends Comparable<T>> void BubbleSort(ArrayList<T> list) {
        for (int i = 0; i < list.size(); i++){
            for (int j = 0; j < list.size() - i - 1; j++){
                if(list.get(j).compareTo(list.get(j + 1)) > 0){
                    T temp = list.get(j);
                    list.set(j, list.get(j + 1));
                    list.set(j + 1, temp);
                }
            }
        }
    }

    /* Selection Sort
     * Worst-case time complexity: O(n^2)
     * Average-case time complexity: O(n^2)
     * Best-case time complexity: O(n^2) - There is no best-case improvement.
     * */
    public static <T extends Comparable<T>> void selectionSort(ArrayList<T> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(minIndex).compareTo(list.get(j)) > 0) {
                    minIndex = j;
                }
            }
            T temp = list.get(i);
            list.set(i, list.get(minIndex));
            list.set(minIndex, temp);
        }
    }

    /* Insertion Sort
     * Worst-case time complexity: O(n^2)
     * Average-case time complexity: O(n^2)
     * Best-case time complexity: O(n) - When the array is already sorted.
     * */
    public static <T extends Comparable<T>> void insertionSort(ArrayList<T> list) {
        int n = list.size();
        for (int i = 1; i < n; i++) {
            T key = list.get(i);
            int j = i - 1;
            while (j >= 0 && list.get(j).compareTo(key) > 0) {
                list.set(j + 1, list.get(j));
                j--;
            }
            list.set(j + 1, key);
        }
    }

    // Bubble Sort with array
    public static <T extends Comparable<T>> void bubbleSortWithArray(T[] arr){
        for (int i = 0; i < arr.length; i++){
            for (int j = 0; j < arr.length - i - 1; j++){
                if(arr[j].compareTo(arr[j + 1]) > 0){
                    T temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    // Time Complexity increases linearly
    public static void linearTime(int n){
        int sum = 0;

        for (int i = 0; i < n; i++){
            sum += i;
        }
    }

    // Time Complexity is const
    public static void constTime(int n){
        int sum = n*(n + 1)/2;
    }
}

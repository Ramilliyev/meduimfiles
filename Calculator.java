import java.util.function.BinaryOperator;

public class Calculator {

    // Higher-order function that returns a function based on the arithmetic operation
    public static BinaryOperator<Integer> getCalculatorOperation(String operation) {
        switch (operation) {
            case "add":
                return Integer::sum;
            case "subtract":
                // (a, b) is parametr list
                // -> expression for dividing parametr list and body
                // a - b is body
                return (a, b) -> a - b;
            case "multiply":
                return (a, b) -> a * b;
            case "divide":
                return (a, b) -> (b != 0) ? a / b : 0; // Avoid division by zero
            default:
                throw new IllegalArgumentException("Unsupported operation: " + operation);
        }
    }

    public static void main(String[] args) {
        // Higher-order function usage
        BinaryOperator<Integer> addOperation = getCalculatorOperation("add");
        BinaryOperator<Integer> subtractOperation = getCalculatorOperation("subtract");
        BinaryOperator<Integer> multiplyOperation = getCalculatorOperation("multiply");
        BinaryOperator<Integer> divideOperation = getCalculatorOperation("divide");

        // Calculations using the returned functions
        System.out.println("Addition: " + addOperation.apply(5, 3));            // Output: 8
        System.out.println("Subtraction: " + subtractOperation.apply(10, 4));   // Output: 6
        System.out.println("Multiplication: " + multiplyOperation.apply(7, 2)); // Output: 14
        System.out.println("Division: " + divideOperation.apply(20, 5));         // Output: 4
    }
}

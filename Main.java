import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        // Creating 3 separate ArrayLists and 1 array for each sorting algorithm
        ArrayList<Integer> list = randomList(10000);
        ArrayList<Integer> list1 = new ArrayList<>(list);
        ArrayList<Integer> list2 = new ArrayList<>(list);
        ArrayList<Integer> list3 = new ArrayList<>(list);
        Integer[] arr4 = list.toArray(new Integer[0]);

        // Printing how much time is spent on each
        System.out.print("Bubble Sort: ");
        
        // () is parametr list
        // -> is expression for dividing parametr list and body
        // Algorithms.BubbleSort(list1) is body
        printTime(() -> Algorithms.BubbleSort(list1));

        System.out.print("\nSelection Sort: ");
        printTime(() -> Algorithms.selectionSort(list2));

        System.out.print("\nInsertion Sort: ");
        printTime(() -> Algorithms.insertionSort(list3));

        System.out.print("\nBubble Sort With Array: ");
        printTime(() -> Algorithms.bubbleSortWithArray(arr4));

        System.out.print("\nLinear Time: ");
        printTime(() -> Algorithms.linearTime(10000000));

        System.out.print("\nLinear Time: ");
        printTime(() -> Algorithms.linearTime(1000000000));

        System.out.print("\nConst Time: ");
        printTime(() -> Algorithms.constTime(10000000));

        System.out.print("\nConst Time: ");
        printTime(() -> Algorithms.constTime(1000000000));

    }

    // For calculating elapsed time
    public static void printTime(Runnable func){
        // The time when code started
        long start = System.currentTimeMillis();

        // Our function
        func.run();

        // The time when code finished
        long end = System.currentTimeMillis();

        // Print the elapsed time in milliseconds
        System.out.println("Time: " + (end - start) + "ms");
    }


    // For fill list with random numbers
    public static ArrayList<Integer> randomList(int size){

        ArrayList<Integer> arr = new ArrayList<>(size);

        Random random = new Random();

        for (int i = 0; i < size; i++){
            arr.add(random.nextInt(10000));
        }

        return arr;
    }

}

